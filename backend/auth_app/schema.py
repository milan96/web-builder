from django.contrib.auth import get_user_model
import graphene
import graphql_jwt
from graphene_django import DjangoObjectType
from graphene_django.views import GraphQLView
from django.utils.decorators import method_decorator
import django_filters
from django.contrib.auth.models import User, Group
from graphene_django.filter import DjangoFilterConnectionField
from graphene import relay, ObjectType, Mutation
# from django.views.decorators.csrf import csrf_exempt

class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Group
        fields = {
            'id': ['exact']
        }

# @method_decorator(csrf_exempt, name='dispatch')
class authGraphQLView(GraphQLView):
    pass

class GroupNode(DjangoObjectType):
    class Meta:
        model = Group
        filterset_class = GroupFilter
        interfaces = (relay.Node, )


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()


class CreateUser(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, username, password, email):
        user = get_user_model()(
            username=username,
            email=email
        )
        user.set_password(password)
        user.save()

        return CreateUser(user=user)


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Query(graphene.ObjectType):
    me = graphene.Field(UserType)
    users = graphene.List(UserType)
    groups = DjangoFilterConnectionField(GroupNode)

    def resolve_users(self, info):
        return get_user_model().objects.all()

    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

        return user


mutations = graphene.Schema(query=Query, mutation=Mutation)
