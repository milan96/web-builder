import graphene
import graphql_jwt
import users.schema
import page.schema


class Query(users.schema.Query, page.schema.Query, graphene.ObjectType):
    pass


class Mutation(users.schema.Mutation, page.schema.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
