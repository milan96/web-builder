ALLOWED_HOSTS = ['localhost', '0.0.0.0', '192.168.1.115']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'wb',
        'USER': 'wb',
        'PASSWORD': 'wb',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}




