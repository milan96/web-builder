"""graphql_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from graphene_django.views import GraphQLView
from auth_app.schema import authGraphQLView, mutations as auth_mutation

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^auth$', authGraphQLView.as_view(graphiql=True, schema=auth_mutation)),
    url(r'^graphql$', GraphQLView.as_view(graphiql=True))
]
