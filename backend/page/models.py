from users.models import User
from django.db import models
from uuid import uuid4

# Create your models here.

class Post(models.Model):
    author = models.OneToOneField(User, on_delete=models.CASCADE)
    uuid = models.UUIDField(
        primary_key=True, default=uuid4, editable=False)
    fields = models.TextField()
    def __str__(self):
        return self.author