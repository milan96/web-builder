from django.contrib.auth import get_user_model
import graphene
import graphql_jwt
from graphene_django import DjangoObjectType
from graphene_django.views import GraphQLView
from django.utils.decorators import method_decorator
import django_filters
from django.contrib.auth.models import User, Group
from graphene_django.filter import DjangoFilterConnectionField
from graphene import relay, ObjectType, Mutation
# from django.views.decorators.csrf import csrf_exempt


class Mutation(graphene.ObjectType):
    pass


class Query(graphene.ObjectType):
    pass


mutations = graphene.Schema(query=Query, mutation=Mutation)
