from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Group, Permission
from users.models import Profile
from questions.models import Pillar


class Command(BaseCommand):
    help = 'Add all groups and first admin user'

    def handle(self, *args, **kwargs):

        permissions = Permission.objects.all()
        admin = Group(
            id=1,
            name='admin',
        )
        admin.save()
        athlete = Group(
            id=2,
            name='athlete',
        )
        athlete.save()
        coach = Group(
            id=3,
            name='coach',
        )
        coach.save()

        for perm in permissions:
            admin.permissions.add(perm)
            if "view" in perm.name:
                coach.permissions.add(perm)
                athlete.permissions.add(perm)

        user = User(
            username='aslavkovic',
            email='aleksandarslavkovic@admin.com',
            first_name='Aleksandar',
            last_name='Slavkovic'
        )
        user.set_password('test1234')
        user.save()

        user2 = User(
            username='afolkman',
            email='aleksafolkman@admin.com',
            first_name='Aleksa',
            last_name='Folkman'
        )
        user2.set_password('test1234')
        user2.save()
        admin.user_set.add(user2)
        profile = Profile(
            user=user,
            user_role=admin
        )
        profile.save()

        profile2 = Profile(
            user=user2,
            user_role=admin
        )
        profile2.save()

        pillars = ['body', 'mind', 'nutrition', 'skill', 'team', 'recovery']

        for x in pillars:
            pillar = Pillar(
                name=x
            )
            pillar.save()
        print('FINISHED!!!')
