from django.db import models
from django.contrib.auth.models import User
from uuid import uuid4
from django.contrib.postgres.fields import ArrayField

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    uuid = models.UUIDField(
        primary_key=True, default=uuid4, editable=False)