from django.contrib.auth import get_user_model
import graphene
from graphene import relay, ObjectType, Mutation
import graphql_jwt
from graphene_django import DjangoObjectType
from .models import Profile
from graphene_django.filter import DjangoFilterConnectionField
import django_filters
from django.contrib.auth.models import User


class Connection(graphene.Connection):
    class Meta:
        abstract = True

    total_count = graphene.Int()

    def resolve_total_count(self, info):
        return self.length

# Filteri


class ProfileFilter(django_filters.FilterSet):
    user = django_filters.CharFilter('user')

    class Meta:
        model = Profile
        fields = {
            'uuid': ['exact'],
        }


class UserFilter(django_filters.FilterSet):
    username = django_filters.CharFilter('username', lookup_expr='contains')
    #
    role = django_filters.CharFilter(
        'profile__user_role__name', lookup_expr='exact')

    class Meta:
        model = Profile
        fields = {
            'uuid': ['exact']
        }

class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Group
        fields = {
            'id': ['exact']
        }


# Zahtevi
class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        filterset_class = UserFilter
        interfaces = (relay.Node, )
        connection_class = Connection


class ProfileNode(DjangoObjectType):
    class Meta:
        model = Profile
        filterset_class = ProfileFilter
        interfaces = (relay.Node, )


# Mutacije

class CRUDuser(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        username = graphene.String()
        password = graphene.String()
        email = graphene.String()
        firstname = graphene.String()
        lastname = graphene.String()
        user_type = graphene.Int()
        course = graphene.UUID()
        id = graphene.UUID()

    def mutate(self, info, username=None, email=None, firstname=None, lastname=None, password=None, user_type=None, id=None, course=None):
        if id:
            if course:
                profile = Profile.objects.get(uuid=id)
                user = profile.user
                course_dest = Course.objects.get(uuid=course)
                course_dest.profile_set.add(profile)

            else:
                profile = Profile.objects.get(uuid=id)
                user = profile.user
                user.first_name = firstname
                user.email = email
                user.last_name = lastname
                user.username = username
                user.save()

        else:
            user = get_user_model()(
                username=username,
                email=email,
                first_name=firstname,
                last_name=lastname
            )
            user.set_password(password)
            user.save()
            profile = Profile(
                user=user
            )
            profile.save()
        return CRUDuser(user=user)


class Mutation(graphene.ObjectType):
    crud_user = CRUDuser.Field()


class Query(graphene.ObjectType):
    me = DjangoFilterConnectionField(UserType)
    users = DjangoFilterConnectionField(UserType)
    profiles = DjangoFilterConnectionField(ProfileNode)

    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

        return user
