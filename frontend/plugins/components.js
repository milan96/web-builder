import Vue from "vue";

import LoginForm from "../components/UI/LoginForm";
import loader from "../components/UI/loader";
import menu from "../components/UI/menu";
import smallLoader from "../components/UI/SmallLoader";

Vue.component("app-login-form", LoginForm);
Vue.component("app-loader", loader);
Vue.component("app-menu", menu);    
Vue.component("app-small-loader", smallLoader);
