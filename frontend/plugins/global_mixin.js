import Vue from "vue";
import { mapGetters, mapActions } from "vuex";

Vue.mixin({
  computed: {
    ...mapGetters("auth", {
      isAuth: "isAuth",
      userData: "userData",
      loading: "loading"
    })
  },
  methods: {
    ...mapActions("auth", {
      logout: "logout",
      initAuth: "initAuth"
    })
  },
  mounted() {}
});
