import Vue from "vue";
import Cookie from "js-cookie";

export default {
  namespaced: true,
  state() {
    return { loading: false, auth_error: false, token: null, user: null };
  },
  getters: {
    isAuth(state) {
      return state.token != null;
    },
    loading(state) {
      return state.loading;
    },
    auth_error(state) {
      return state.auth_error;
    },
    userData(state) {
      return state.user;
    }
  },
  mutations: {
    settoken(state, data) {
      state.token = data;
    },
    toggle_loading(state) {
      state.loading = !state.loading;
    },
    auth_error(state) {
      state.auth_error = true;
    },
    setUser(state, data) {
      state.user = data;
    }
  },
  actions: {
    async login(store, data) {
      let ajaxData = {
        query: `mutation($username: String!, $password: String!) {
              tokenAuth(username: $username, password: $password) {
                  token
              }
          }`,
        variables: { username: data.username, password: data.password }
      };

      const result = await this.$axios
        .post(`${process.env.API_URL}/auth`, ajaxData)
        .then(response => {
          if (!response.data.data.tokenAuth) {
            return store.commit("auth_error");
          }
          store.commit("settoken", `JWT ${response.data.data.tokenAuth.token}`);
          if (process.client) {
            localStorage.setItem(
              "token",
              `JWT ${response.data.data.tokenAuth.token}`
            );
          }
          localStorage.setItem(
            "token",
            `JWT ${response.data.data.tokenAuth.token}`
          );
          Cookie.set("token", `JWT ${response.data.data.tokenAuth.token}`);
          this.$axios.defaults.headers.common[
            "Authorization"
          ] = `JWT ${response.data.data.tokenAuth.token}`;
          store.dispatch("fetchUser");
        })
        .catch(e => {
          return Promise.reject(e);
        });
    },
    async initAuth(store, req) {
      if (req) {
        if (!req.headers.cookie) {
          return;
        }
        const cookie_token = req.headers.cookie
          .split(";")
          .find(c => c.trim().startsWith("token="));
        if (!cookie_token) {
          return;
        }
        const token = cookie_token.split("=")[1];
        store.commit("settoken", token);
        this.$axios.defaults.headers.common["Authorization"] = token;
      } else if (process.client) {
        const token = localStorage.getItem("token");
        store.commit("settoken", token);
        this.$axios.defaults.headers.common["Authorization"] = token;
      }
      await store.dispatch("fetchUser");
      return true;
    },
    async clear() {
      if (process.client) {
        await localStorage.removeItem("token");
      }
      await Cookie.remove("token");
    },
    async logout(store) {
      await store.dispatch("clear");
      window.location.reload(true);
    },
    async fetchUser(store) {
      if (!process.client) {
        return;
      }

      const ajaxData = {
        query: `
        query {
          me {
            username
            id
          }
        }`
      };

      const user = await this.$axios.post(
        `${process.env.API_URL}/auth`,
        ajaxData
      );

      await store.commit("setUser", {
        ...user.data.data.me
      });

      return true;
    }
  }
};
