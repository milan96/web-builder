import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import auth from "./auth";
import page from "./page"

export const store = new Vuex.Store({
  modules: {
    auth,
    page
  },
  strict: process.env.NODE_ENV !== "production"
});
