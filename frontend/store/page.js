export default {
    namespaced: true,
    state() {
        return {
            rows: [],
            mobile_help: null,
            draged_index: null,
            target: null
        };
    },
    getters: {
        rows(state) {
            return state.rows
        },
        draged_index(state) {
            return state.draged_index
        },
        mobile_help(state) {
            return state.mobile_help
        }
    },
    mutations: {
        add_row(state, data) {
            state.rows = [...state.rows, { type: data.type, content: null }]
        },
        set_drag(state, data) {
            state.draged_index = data
        },
        set_target(state, data) {
            state.target = data
        },
        set_rows(state, data) {
            state.rows = data
        },
        set_page_data(state, data) {
            for (const key in data) {
                state.rows[Number(key)].content = data[key]
            }
        },
        reverse_rows(state) {
            let rows = [...state.rows]
            rows.reverse()
            state.rows = [...rows]
        },
        delete_row(state, data) {
            let rows = [...state.rows]

            rows.splice(data, 1)

            state.rows = [...rows]
            state.mobile_help = null
        },
        // replace_blocks(state, data) {
        //     let help = []
        //     if (state.mobile_help) {
        //         console.log('IMA MOBILE U STORU');

        //         help = [...state.mobile_help]
        //     } else {


        //         help = [...state.rows]
        //     }
        //     let copy = [...state.rows]
        //     copy[data[0]] = help[data[1]]
        //     copy[data[1]] = help[data[0]]
        //     state.mobile_help = [...copy]


        // },
        set_mobile(state, data) {

            state.mobile_help = [...data]
        },
        null_mobile_help(state) {
            state.mobile_help = null
        }

    },
    actions: {
        async push_to(store, data) {
            let help = []
            if (store.state.mobile_help) {
                help = [...store.state.mobile_help]
            } else {
                help = [...store.state.rows]
            }


            setTimeout(() => {
                let draggable_el = help[data.from]
                help.splice(data.from, 1)
                help.splice(data.to, 0, draggable_el);
                store.commit("set_mobile", [...help])
            }, 10);



        },

        async touch_out(store) {
            if (store.state.mobile_help) {
                await store.commit("set_rows", null)
                setTimeout(() => {
                    store.commit("set_rows", [...store.state.mobile_help])
                }, 10);
            }
        },
        async replace_rows(store, data) {
            let old_row = [...store.state.rows]
            let new_row = []
            for (let i = 0; i < old_row.length; i++) {
                if (i != store.state.draged_index) {
                    new_row.push(old_row[i])
                }
            }
            new_row.splice(store.state.target, 0, store.state.rows[store.state.draged_index])
            store.commit("set_rows", null)
            setTimeout(() => {
                store.commit("set_rows", [...new_row])
            }, 0);



        }
    }
};
