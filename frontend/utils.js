export const graphqlURL = query => {
    let output = query.replace(/\n/g, "");
    output = `graphql?query=${output.replace(/\s/g, " ")}`;
    return output;
  };
  
  export const graphqlMutation = query => {
    let output = query.replace(/\n/g, "");
    output = output.replace(/\s/g, "");
    return output;
  };
  